const productList = document.getElementsByClassName("product");

const clickHandler = event => {
  const productNameTwoLines = event.currentTarget
                              .getElementsByClassName("Product-name")[0]
                              .getElementsByTagName("span")
  console.log(`${productNameTwoLines[0].textContent} ${productNameTwoLines[1].textContent}`);                              
};

[...productList].forEach(product => {
  product.addEventListener("click", clickHandler);
});
